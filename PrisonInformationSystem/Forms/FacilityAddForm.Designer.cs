﻿using PrisonInformationSystem.Forms.Controls;

namespace PrisonInformationSystem.Forms
{
    partial class FacilityAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveButton = new System.Windows.Forms.Button();
            this.titleLabel = new System.Windows.Forms.Label();
            this.facilityTextBox = new PrisonInformationSystem.Forms.Controls.ExtendedTextBox();
            this.SuspendLayout();
            // 
            // saveButton
            // 
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.Location = new System.Drawing.Point(44, 75);
            this.saveButton.Name = "saveButton";
            this.saveButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.saveButton.Size = new System.Drawing.Size(150, 40);
            this.saveButton.TabIndex = 8;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.SaveButtonClick);
            // 
            // titleLabel
            // 
            this.titleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(0, 0);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(238, 34);
            this.titleLabel.TabIndex = 6;
            this.titleLabel.Text = "Add new facility";
            this.titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // facilityTextBox
            // 
            this.facilityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.facilityTextBox.Location = new System.Drawing.Point(32, 33);
            this.facilityTextBox.Margin = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.facilityTextBox.Name = "facilityTextBox";
            this.facilityTextBox.OrigColorHolder = System.Drawing.SystemColors.WindowText;
            this.facilityTextBox.Placeholder = "Title";
            this.facilityTextBox.PlaceholderColor = System.Drawing.Color.Empty;
            this.facilityTextBox.Size = new System.Drawing.Size(177, 31);
            this.facilityTextBox.TabIndex = 9;
            // 
            // FacilityAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(238, 127);
            this.Controls.Add(this.facilityTextBox);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.titleLabel);
            this.Name = "FacilityAddForm";
            this.Text = "FacilityAddForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label titleLabel;
        private ExtendedTextBox facilityTextBox;
    }
}