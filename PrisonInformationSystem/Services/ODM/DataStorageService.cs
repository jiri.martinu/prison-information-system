using System.Xml.Linq;

namespace PrisonInformationSystem.Services.ODM
{
    public class DataStorageService
    {
        public XElement Root { get; set; }

        /// <summary>
        /// Constructor. Loads data.xml file to XElement
        /// </summary>   
        public DataStorageService() 
        {
            Root = XElement.Load(@"data.xml");
        }

        /// <summary>
        /// Saves modifications in data.xml
        /// </summary>     
        public void Flush()
        {
            Root.Save(@"data.xml");
        }
    }
}