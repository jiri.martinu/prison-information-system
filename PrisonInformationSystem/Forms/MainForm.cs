﻿using System;
using System.Windows.Forms;
using Autofac;
using PrisonInformationSystem.ExtensionMethods;
using PrisonInformationSystem.Forms;
using PrisonInformationSystem.Managers;
using PrisonInformationSystem.Models;
using PrisonInformationSystem.Services;

namespace PrisonInformationSystem
{
    public partial class MainForm : Form
    {
        public FacilityControlGenerator FacilityControlGenerator { get; set; }

        public FacilityManager FacilityManager { get; set; }

        public FacilityAddForm FacilityAddForm { get; set; }

        public ILifetimeScope Scope { get; set; }

        public MainForm(
            FacilityControlGenerator facilityControlGenerator, 
            FacilityManager facilityManager,
            ILifetimeScope scope
        ) {
            FacilityControlGenerator = facilityControlGenerator;
            FacilityManager = facilityManager;
            Scope = scope;

            InitializeComponent();
        }

        private void ExitButtonClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainFormLoad(object sender, EventArgs e)
        {
            RenderFacilities();
        }

        public void RenderFacilities()
        {
            flowLayoutPanel1.Controls.Clear();

            FacilityManager.GetAll().ForEach((item, index) => flowLayoutPanel1.Controls.Add(
                FacilityControlGenerator.CreateFacilityUI(item as Facility, index)
            ));
        }

        private void NewButtonClick(object sender, EventArgs e)
        {
            FacilityAddForm addForm = Scope.Resolve<FacilityAddForm>();

            if (FacilityManager.GetAll().Count >= 4) {
                FacilityLimitReached();

                return;
            }

            if (addForm.ShowDialog() == DialogResult.OK) {
                RenderFacilities();
            }
        }

        private void FacilityLimitReached()
        {
            MessageBox.Show(
                "You have to remove some facility to add new one!", 
                "Facility limit reached", 
                MessageBoxButtons.OK
            );
        }
    }
}
