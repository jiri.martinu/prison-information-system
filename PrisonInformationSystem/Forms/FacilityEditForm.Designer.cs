﻿namespace PrisonInformationSystem.Forms
{
    partial class FacilityEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainTitle = new System.Windows.Forms.Label();
            this.prisonersGridView = new System.Windows.Forms.DataGridView();
            this.exitButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.executeButton = new System.Windows.Forms.Button();
            this.editToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.executeToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.moveButton = new System.Windows.Forms.Button();
            this.moveToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.newButton = new System.Windows.Forms.Button();
            this.newToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.facilityTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.prisonersGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // mainTitle
            // 
            this.mainTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.mainTitle.Location = new System.Drawing.Point(102, 1);
            this.mainTitle.Margin = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.mainTitle.Name = "mainTitle";
            this.mainTitle.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.mainTitle.Size = new System.Drawing.Size(98, 40);
            this.mainTitle.TabIndex = 0;
            this.mainTitle.Text = "Facility:";
            this.mainTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // prisonersGridView
            // 
            this.prisonersGridView.AllowUserToAddRows = false;
            this.prisonersGridView.AllowUserToDeleteRows = false;
            this.prisonersGridView.AllowUserToOrderColumns = true;
            this.prisonersGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.prisonersGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.prisonersGridView.Location = new System.Drawing.Point(19, 51);
            this.prisonersGridView.Margin = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.prisonersGridView.Name = "prisonersGridView";
            this.prisonersGridView.ReadOnly = true;
            this.prisonersGridView.Size = new System.Drawing.Size(350, 230);
            this.prisonersGridView.TabIndex = 1;
            // 
            // exitButton
            // 
            this.exitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.Location = new System.Drawing.Point(121, 294);
            this.exitButton.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.exitButton.Name = "exitButton";
            this.exitButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.exitButton.Size = new System.Drawing.Size(200, 40);
            this.exitButton.TabIndex = 2;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.ExitButtonClick);
            // 
            // editButton
            // 
            this.editButton.BackColor = System.Drawing.Color.Orange;
            this.editButton.FlatAppearance.BorderSize = 0;
            this.editButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.editButton.Location = new System.Drawing.Point(378, 111);
            this.editButton.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(50, 50);
            this.editButton.TabIndex = 3;
            this.editButton.Text = "✎";
            this.editButton.UseVisualStyleBackColor = false;
            this.editButton.Click += new System.EventHandler(this.EditButtonClick);
            // 
            // executeButton
            // 
            this.executeButton.BackColor = System.Drawing.Color.Red;
            this.executeButton.FlatAppearance.BorderSize = 0;
            this.executeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.executeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.executeButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.executeButton.Location = new System.Drawing.Point(378, 171);
            this.executeButton.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(50, 50);
            this.executeButton.TabIndex = 4;
            this.executeButton.Text = "💀";
            this.executeButton.UseVisualStyleBackColor = false;
            this.executeButton.Click += new System.EventHandler(this.ExecuteButtonClick);
            // 
            // moveButton
            // 
            this.moveButton.BackColor = System.Drawing.Color.RoyalBlue;
            this.moveButton.FlatAppearance.BorderSize = 0;
            this.moveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.moveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.moveButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.moveButton.Location = new System.Drawing.Point(378, 231);
            this.moveButton.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.moveButton.Name = "moveButton";
            this.moveButton.Size = new System.Drawing.Size(50, 50);
            this.moveButton.TabIndex = 5;
            this.moveButton.Text = "⮩";
            this.moveButton.UseVisualStyleBackColor = false;
            this.moveButton.Click += new System.EventHandler(this.MoveButtonClick);
            // 
            // newButton
            // 
            this.newButton.BackColor = System.Drawing.Color.LimeGreen;
            this.newButton.FlatAppearance.BorderSize = 0;
            this.newButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.newButton.Location = new System.Drawing.Point(378, 51);
            this.newButton.Margin = new System.Windows.Forms.Padding(0);
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size(50, 50);
            this.newButton.TabIndex = 6;
            this.newButton.Text = "+";
            this.newButton.UseVisualStyleBackColor = false;
            this.newButton.Click += new System.EventHandler(this.NewButtonClick);
            // 
            // facilityTextBox
            // 
            this.facilityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.facilityTextBox.Location = new System.Drawing.Point(192, 10);
            this.facilityTextBox.Margin = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.facilityTextBox.Name = "facilityTextBox";
            this.facilityTextBox.Size = new System.Drawing.Size(177, 31);
            this.facilityTextBox.TabIndex = 7;
            this.facilityTextBox.TextChanged += new System.EventHandler(this.FacilityTextBoxTextChanged);
            // 
            // FacilityEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 344);
            this.Controls.Add(this.facilityTextBox);
            this.Controls.Add(this.newButton);
            this.Controls.Add(this.moveButton);
            this.Controls.Add(this.executeButton);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.prisonersGridView);
            this.Controls.Add(this.mainTitle);
            this.Name = "FacilityEditForm";
            this.Text = "FacilityEditForm";
            this.Load += new System.EventHandler(this.FacilityEditFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.prisonersGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label mainTitle;
        private System.Windows.Forms.DataGridView prisonersGridView;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button executeButton;
        private System.Windows.Forms.ToolTip editToolTip;
        private System.Windows.Forms.ToolTip executeToolTip;
        private System.Windows.Forms.Button moveButton;
        private System.Windows.Forms.ToolTip moveToolTip;
        private System.Windows.Forms.Button newButton;
        private System.Windows.Forms.ToolTip newToolTip;
        private System.Windows.Forms.TextBox facilityTextBox;
    }
}