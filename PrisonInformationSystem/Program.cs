﻿using System;
using System.Windows.Forms;
using Autofac;
using PrisonInformationSystem.Forms;
using PrisonInformationSystem.Managers;
using PrisonInformationSystem.Services;
using PrisonInformationSystem.Services.ODM;

namespace PrisonInformationSystem
{
    public static class Program
    {
        public static IContainer InitializeContainer()
        {
            return new ContainerBuilder().ConfigureServices().Build();
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            using (var scope = InitializeContainer().BeginLifetimeScope())
            {
                Application.Run(scope.Resolve<MainForm>());
            }
        }

        /// <summary>
        /// Register individual services
        /// </summary>
        private static ContainerBuilder ConfigureServices(this ContainerBuilder builder)
        {
            builder.RegisterType<MainForm>();
            builder.RegisterType<FacilityEditForm>();
            builder.RegisterType<PrisonerAddEditForm>();
            builder.RegisterType<PrisonerMoveForm>();
            builder.RegisterType<FacilityAddForm>();

            builder.RegisterType<FacilityManager>();
            builder.RegisterType<PrisonerManager>();

            builder.RegisterType<FacilityPrisonerFacade>();

            builder.RegisterType<FacilityControlGenerator>();
            builder.RegisterType<AttributesReader>();
            builder.RegisterType<ObjectHydratator>();
            builder.RegisterType<EntityManager>().SingleInstance();
            builder.RegisterType<DataStorageService>();

            return builder;
        }
    }
}
