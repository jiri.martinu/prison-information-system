﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Autofac;
using PrisonInformationSystem;
using PrisonInformationSystem.Managers;
using PrisonInformationSystem.Models;
using PrisonInformationSystem.Services;

namespace PrisonInformationSystemTests
{
    [TestClass]
    public class FacilityPrisonerFacadeTest
    {
        public IContainer Container;

        public FacilityManager FacilityManager;

        public PrisonerManager PrisonerManager;

        public FacilityPrisonerFacade FacilityPrisonerFacade;

        public void Initialize()
        {
            File.Copy(@"data_recovery.xml", @"data.xml", true);

            Container = Program.InitializeContainer();
            FacilityManager = Container.Resolve<FacilityManager>();
            PrisonerManager = Container.Resolve<PrisonerManager>();
            FacilityPrisonerFacade = Container.Resolve<FacilityPrisonerFacade>();
        }

        [TestMethod]
        public void TestMoveOfPrisoner()
        {
            Initialize();

            Prisoner prisoner = PrisonerManager.FindById(1) as Prisoner;
            Facility to = FacilityManager.FindById(2) as Facility;

            FacilityPrisonerFacade.MovePrisoner(to, prisoner);

            File.Copy(@"data.xml", @"data_post_prisoner_move.xml", true);
        }
    }
}
