﻿using System;
using System.Collections;
using System.Reflection;
using System.Xml.Linq;
using PrisonInformationSystem.Models;
using PrisonInformationSystem.Models.ODM;
using PrisonInformationSystem.Utils.ODM;
using static PrisonInformationSystem.Utils.ODM.PersistentAttribute;

namespace PrisonInformationSystem.Services.ODM
{
    public class ObjectHydratator
    {
        public AttributesReader AttributesReader { get; set; }

        public ObjectHydratator(AttributesReader attributesReader)
        {
            AttributesReader = attributesReader;
        }

        public XElement Extract(BaseEntity input)
        {
            XElement element = new XElement(input.GetType().Name);

            foreach ((PropertyInfo, PersistentAttribute) property in AttributesReader.GetListPersistentProperties(input.GetType())) {
                if (property.Item2.IsId == true) {
                    element.Add(new XAttribute("id", GetPropertyValue(input, property.Item1.Name)));

                    continue;
                }

                if (property.Item2.RelationType != RelationTypeNames.NotRelation) {
                    object related = GetPropertyValue(input, property.Item1.Name);

                    if (property.Item2.RelationType == RelationTypeNames.OneToMany) {
                        XElement collectionElement = new XElement(property.Item1.Name);

                        foreach (var child in (related as IList)) {
                            collectionElement.Add(
                                new XElement(
                                    child.GetType().Name,
                                    GetPropertyValue(child, "Id")
                                )
                            );
                        }

                        element.Add(collectionElement);
                    } else {
                        element.Add(new XElement(
                                property.Item1.Name,
                                GetPropertyValue(related, "Id")
                            )
                        );
                    }

                    continue;
                }

                element.Add(new XElement(property.Item1.Name, GetPropertyValue(input, property.Item1.Name)));
            }

            return element;
        }

        public BaseEntity Hydrate(XElement input)
        {
            BaseEntity entity = Activator.CreateInstance(Type.GetType("PrisonInformationSystem.Models." + input.Name.LocalName)) as BaseEntity;

            foreach ((PropertyInfo, PersistentAttribute) property in AttributesReader.GetListPersistentProperties(entity.GetType())) {
                if (property.Item2.IsId == true) {
                    SetPropertyValue(entity, "Id", input.FirstAttribute.Value);

                    continue;
                }

                if (property.Item2.RelationType != RelationTypeNames.NotRelation) {
                    Relation relation = new Relation
                    {
                        PropertyInfo = entity.GetType().GetProperty(property.Item1.Name)
                    };

                    if (property.Item2.RelationType == RelationTypeNames.OneToMany) {
                        foreach (XElement child in input.Elements(relation.PropertyInfo.Name).Elements()) {
                            relation.ChildIds.Add(int.Parse(child.Value));
                        }
                    } else {
                        relation.ChildIds.Add(int.Parse(input.Element(relation.PropertyInfo.Name).Value));
                    }

                    entity.Relations.Add(relation);

                    continue;
                }

                SetPropertyValue(
                    entity, 
                    property.Item1.Name, 
                    input.Element(property.Item1.Name).Value
                );
            }

            return entity;
        }

        public object GetPropertyValue<TObject>(TObject input, string propName)
        {
            return input.GetType().GetProperty(propName).GetValue(input);
        }

        public void SetPropertyValue<TObject, TValue>(TObject input, string propName, TValue value)
        {
            PropertyInfo prop = input.GetType().GetProperty(propName);

            prop.SetValue(input, Convert.ChangeType(value, prop.PropertyType));
        }
    }
}
