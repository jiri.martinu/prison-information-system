﻿using System;
using System.Linq;
using System.Windows.Forms;
using PrisonInformationSystem.Managers;
using PrisonInformationSystem.Models;
using PrisonInformationSystem.Services;

namespace PrisonInformationSystem.Forms
{
    public partial class PrisonerMoveForm : Form
    {
        public Prisoner Prisoner { get; set; }

        public FacilityManager FacilityManager { get; set; }

        public FacilityPrisonerFacade FacilityPrisonerFacade { get; set; }

        public PrisonerMoveForm(
            Prisoner prisoner,
            FacilityManager facilityManager,
            FacilityPrisonerFacade facilityPrisonerFacade
        ) {
            Prisoner = prisoner;
            FacilityManager = facilityManager;
            FacilityPrisonerFacade = facilityPrisonerFacade;

            InitializeComponent();
        }

        private void MoveButtonClick(object sender, EventArgs e)
        {
            if (facilityComboBox.SelectedItem == null) {
                MessageBox.Show(
                    "You have choose facility!", 
                    "Facility not selected", 
                    MessageBoxButtons.OK
                );

                return;
            }

            FacilityPrisonerFacade.MovePrisoner(facilityComboBox.SelectedItem as Facility, Prisoner);

            DialogResult = DialogResult.OK;
        }

        private void PrisonerMoveFormLoad(object sender, EventArgs e)
        {
            facilityComboBox.Items.AddRange(
                FacilityManager.GetAll().Where(f => 
                    !(f as Facility).HasPrisoner(Prisoner)
                ).ToList().ToArray()
            );
        }
    }
}
