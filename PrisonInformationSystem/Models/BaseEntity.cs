﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using PrisonInformationSystem.Models.ODM;
using PrisonInformationSystem.Utils.ODM;

namespace PrisonInformationSystem.Models
{
    public class BaseEntity : INotifyPropertyChanged
    {
        [Persistent(IsId = true)]
        public int Id { get; set; }

        public IList<Relation> Relations = new List<Relation>();

        public void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
