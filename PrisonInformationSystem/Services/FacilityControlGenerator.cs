﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Autofac;
using PrisonInformationSystem.ExtensionMethods;
using PrisonInformationSystem.Forms;
using PrisonInformationSystem.Forms.Controls;
using PrisonInformationSystem.Managers;
using PrisonInformationSystem.Models;

namespace PrisonInformationSystem.Services
{
    public class FacilityControlGenerator
    {
        public FacilityManager FacilityManager { get; set; }

        public ILifetimeScope Scope { get; set; }

        public FacilityControlGenerator(FacilityManager facilityManager, ILifetimeScope scope)
        {
            FacilityManager = facilityManager;
            Scope = scope;
        }

        public FlowLayoutPanel CreateFacilityUI(Facility facility, int panelIndex)
        {
            Label label = new Label
            {
                Dock = DockStyle.Bottom,
                Font = new Font("Microsoft Sans Serif", 15.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))),
                Location = new Point(0, 0),
                Margin = new Padding(0),
                Name = "label",
                Size = new Size(120, 100),
                TabIndex = 6,
                Text = "(Id: " + facility.Id + ") " + facility.Title.Truncate(20),
                TextAlign = ContentAlignment.MiddleCenter
            };

            Button editButton = new Button
            {
                BackColor = Color.Orange,
                FlatStyle = FlatStyle.Flat,
                Font = new Font("Microsoft Sans Serif", 15.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))),
                ForeColor = SystemColors.ControlLightLight,
                Location = new Point(130, 0),
                Margin = new Padding(0, 25, 0, 0),
                Name = "editButton",
                Size = new Size(50, 50),
                TabIndex = 7,
                Text = "✎",
                UseVisualStyleBackColor = false,
            };

            editButton.FlatAppearance.BorderSize = 0;
            editButton.Click += new EventHandler(EditButtonClick);

            Button deleteButton = new Button
            {
                BackColor = Color.Red,
                FlatStyle = FlatStyle.Flat,
                Font = new Font("Microsoft Sans Serif", 15.75F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0))),
                ForeColor = SystemColors.ControlLightLight,
                Location = new Point(190, 0),
                Margin = new Padding(10, 25, 0, 0),
                Name = "deleteButton",
                Size = new Size(50, 50),
                TabIndex = 8,
                Text = "🗙",
                UseVisualStyleBackColor = false,
            };

            deleteButton.FlatAppearance.BorderSize = 0;
            deleteButton.Click += new EventHandler(DeleteButtonClick);

            Size panelSize = new Size(242, 100);

            ExtendedFlowLayoutPanel panel = new ExtendedFlowLayoutPanel
            {
                BackColor = Color.Azure,
                Size = new Size(242, 100),
                Location = new Point(0, 0 + panelSize.Height * panelIndex),
                Name = "flowLayoutPanel" + panelIndex.ToString(),
                Padding = new Padding(0, 0, 0, 0),
                Margin = new Padding(10, 10, 0, 0),
                TabIndex = 3,
                ExtraData = facility,
            };

            panel.Controls.Add(label);
            panel.Controls.Add(editButton);
            panel.Controls.Add(deleteButton);

            return panel;
        }

        private void EditButtonClick(object sender, EventArgs e)
        {
            FacilityEditForm editForm = Scope.Resolve<FacilityEditForm>(
                new TypedParameter(typeof(Facility), GetExtraData(sender))
            );

            MainForm parentForm = GetParentForm(sender);

            parentForm.Hide();

            if (new List<DialogResult> { DialogResult.OK, DialogResult.Cancel }.Contains(editForm.ShowDialog()) == true) {
                // There is small chance that title of Facility was changed and 
                // this is only way how to update "list" on MainForm
                parentForm.RenderFacilities();
                parentForm.Show();
            }
        }

        private void DeleteButtonClick(object sender, EventArgs e)
        {
            FacilityManager.Remove(GetExtraData(sender) as BaseEntity, true);

            GetParentForm(sender).RenderFacilities();
        }

        private MainForm GetParentForm(object sender)
        {
            return ((ExtendedFlowLayoutPanel)((Control)sender).Parent).Parent.Parent as MainForm;
        }

        private Facility GetExtraData(object sender)
        {
            return ((ExtendedFlowLayoutPanel)((Control)sender).Parent).ExtraData as Facility;
        }
    }
}
