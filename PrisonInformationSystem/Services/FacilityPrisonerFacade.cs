using System.Linq;
using PrisonInformationSystem.Managers;
using PrisonInformationSystem.Models;

namespace PrisonInformationSystem.Services
{
    /// <summary>
    /// Solves issues with relations between Facility and Prisoner
    /// </summary>
    public class FacilityPrisonerFacade
    {
        public FacilityManager FacilityManager { get; set; }

        public PrisonerManager PrisonerManager { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public FacilityPrisonerFacade(FacilityManager facilityManager, PrisonerManager prisonerManager)
        {
            FacilityManager = facilityManager;
            PrisonerManager = prisonerManager;
        }

        public Facility AddPrisonerToFacility(Facility facility, Prisoner prisoner)
        {
            prisoner = PrisonerManager.Add(prisoner) as Prisoner;

            facility.Prisoners.Add(prisoner);
            facility = FacilityManager.Update(facility) as Facility;

            return facility;
        }

        public void RemovePrisonerWithRelations(Prisoner prisoner)
        {
            Facility facility = FacilityManager.GetAll()
                .Where(f => (f as Facility)
                .HasPrisoner(prisoner))
                .FirstOrDefault() as Facility;

            facility.Prisoners.Remove(prisoner);

            FacilityManager.Update(facility);
            PrisonerManager.Remove(prisoner);
        }

        public void MovePrisoner(Facility to, Prisoner prisoner)
        {
            Facility from = FacilityManager.EntityManager.EntitySets[typeof(Facility)]
                .Where(f => (f as Facility).HasPrisoner(prisoner)).FirstOrDefault() as Facility;

            if (from is Facility) {
                from.Prisoners.Remove(prisoner);
                FacilityManager.Update(from);

                to.Prisoners.Add(prisoner);
                FacilityManager.Update(to);
            }
        }
    }
}