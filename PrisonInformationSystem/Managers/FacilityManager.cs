using PrisonInformationSystem.Models;
using PrisonInformationSystem.Services.ODM;

namespace PrisonInformationSystem.Managers
{
    public class FacilityManager : BaseManager<Facility>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public FacilityManager(EntityManager entityManager) : base(entityManager)
        {

        }
    }
}