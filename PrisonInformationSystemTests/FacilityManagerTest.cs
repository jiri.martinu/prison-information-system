﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Autofac;
using PrisonInformationSystem;
using PrisonInformationSystem.Managers;
using PrisonInformationSystem.Models;
using PrisonInformationSystem.Models.ODM;

namespace PrisonInformationSystemTests
{
    [TestClass]
    public class FacilityManagerTest
    {
        public IContainer Container;

        public FacilityManager FacilityManager;

        public PrisonerManager PrisonerManager;

        public void Initialize()
        {
            File.Copy(@"data_recovery.xml", @"data.xml", true);

            Container = Program.InitializeContainer();
            FacilityManager = Container.Resolve<FacilityManager>();
            PrisonerManager = Container.Resolve<PrisonerManager>();
        }

        [TestMethod]
        public void TestPresenceOfFirstObject()
        {
            Initialize();

            Facility facility = FacilityManager.FindById(1) as Facility;

            Assert.AreEqual("Pankrác", facility.Title);
        }

        [TestMethod]
        public void TestAdd()
        {
            Initialize();

            Facility facility = new Facility { Title = "Pankrác" };

            facility.Relations.Add(new Relation
            {
                PropertyInfo = facility.GetType().GetProperty("Prisoners")
            });

            facility = FacilityManager.Add(facility) as Facility;

            Assert.AreEqual(3, FacilityManager.GetAll().Count);
            Assert.AreEqual(3, facility.Id);

            File.Copy(@"data.xml", @"data_post_facility_add.xml", true);
        }

        [TestMethod]
        public void TestUpdate()
        {
            Initialize();

            Prisoner prisoner = PrisonerManager.Add(new Prisoner { Name = "John", Surname = "Dope" }) as Prisoner;

            Facility facility = FacilityManager.FindById(1) as Facility;

            facility.Prisoners.Add(prisoner);
            facility = FacilityManager.Update(facility) as Facility;

            Assert.AreEqual(3, facility.Prisoners.Count);
            Assert.AreEqual("Dope", facility.Prisoners[facility.Prisoners.Count - 1].Surname);

            File.Copy(@"data.xml", @"data_post_facility_update.xml", true);
        }

        [TestMethod]
        public void TestRemove()
        {
            Initialize();

            Facility facility = FacilityManager.FindById(1) as Facility;

            FacilityManager.Remove(facility);

            Assert.AreEqual(1, FacilityManager.GetAll().Count);
            Assert.AreEqual(2, FacilityManager.GetAll()[0].Id);

            File.Copy(@"data.xml", @"data_post_facility_remove.xml", true);
        }

        [TestMethod]
        public void TestRemoveWithCascade()
        {
            Initialize();

            Facility facility = FacilityManager.FindById(1) as Facility;

            FacilityManager.Remove(facility, true);

            Assert.AreEqual(1, FacilityManager.GetAll().Count);
            Assert.AreEqual(2, FacilityManager.GetAll()[0].Id);
            Assert.AreEqual(0, PrisonerManager.GetAll().Count);

            File.Copy(@"data.xml", @"data_post_facility_remove_with_cascade.xml", true);
        }
    }
}
