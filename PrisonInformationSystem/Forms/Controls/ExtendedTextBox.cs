﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PrisonInformationSystem.Forms.Controls
{
    class ExtendedTextBox : TextBox
    {
        public string Placeholder { get; set; }

        public Color PlaceholderColor { get; set; }

        public Color OrigColorHolder { get; set; }

        public ExtendedTextBox()
        {
            GotFocus += new EventHandler(OnGotFocus);
            LostFocus += new EventHandler(OnLostFocus);
            HandleCreated += new EventHandler(Initialize);
            TextChanged += new EventHandler(OnTextChange);
        }

        public void OnTextChange(object sender, EventArgs e)
        {
            if (Text == Placeholder) {
                ForeColor = PlaceholderColor;
            } else {
                ForeColor = OrigColorHolder;
            }
        }

        public void Initialize(object sender, EventArgs e)
        {
            OrigColorHolder = ForeColor;
            OnLostFocus(sender, e);
        }
        
        public void OnGotFocus(object sender, EventArgs e)
        {
            ForeColor = OrigColorHolder;

            if (Text == Placeholder) {
                Text = "";
            }
        }

        public void OnLostFocus(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Text)) {
                Text = Placeholder;
                ForeColor = PlaceholderColor;
            }
        }

        public bool IsEmpty()
        {
            return string.IsNullOrWhiteSpace(Text) || Text == Placeholder;
        }
    }
}
