﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace PrisonInformationSystem.Models.ODM
{
    public class Relation
    {
        public PropertyInfo PropertyInfo { get; set; }

        public IList<int> ChildIds { get; set; } = new List<int>();

        public bool IsCollection()
        {
            return PropertyInfo.PropertyType.Name.Contains("IList") == true;
        }

        public Type GetUnderLayingType()
        {
            return PropertyInfo.PropertyType.GenericTypeArguments[0];
        }
    }
}
