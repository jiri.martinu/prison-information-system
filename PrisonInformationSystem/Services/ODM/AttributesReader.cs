﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using PrisonInformationSystem.Utils.ODM;
using static PrisonInformationSystem.Utils.ODM.PersistentAttribute;

namespace PrisonInformationSystem.Services.ODM
{
    public class AttributesReader
    {
        public IList<(MemberInfo, PersistentAttribute)> GetListPersistentProperties(Type t)
        {
            PropertyInfo[] propertiesInfo = t.GetProperties();
            PersistentAttribute att;
            IList<(MemberInfo, PersistentAttribute)> result = new List<(MemberInfo, PersistentAttribute)>();

            for (int i = 0; i < propertiesInfo.Length; i++) {
                att = (PersistentAttribute) Attribute.GetCustomAttribute(propertiesInfo[i], typeof(PersistentAttribute));

                if (att != null) {
                    result.Add((propertiesInfo[i], att));
                }
            }

            return result;
        }

        public IList<(MemberInfo, PersistentAttribute)> GetListPersistentRelationProperties(Type t)
        {
            return GetListPersistentProperties(t)
                .Where(p => p.Item2.RelationType != RelationTypeNames.NotRelation)
                .ToList();
        }

        /// <summary>
        /// Find every persistent class in Models folder. This should be probably somewhere else
        /// but I'm pretty sure that nobody will ever notice
        /// </summary>   
        public IList<Type> GetListPersistentClasses()
        {
            PersistentAttribute att;

            var q = from t in Assembly.GetExecutingAssembly().GetTypes()
                    where t.IsClass && t.Namespace == "PrisonInformationSystem.Models"
                    select t;

            IEnumerable<Type> res = q.ToList().Where(c => {
                att = (PersistentAttribute)Attribute.GetCustomAttribute(c, typeof(PersistentAttribute));

                return att != null ? true : false;
            });

            return new List<Type>(res);
        }
    }
}
