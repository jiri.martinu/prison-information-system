using PrisonInformationSystem.Models;
using PrisonInformationSystem.Services.ODM;

namespace PrisonInformationSystem.Managers
{
    public class PrisonerManager : BaseManager<Prisoner>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public PrisonerManager(EntityManager entityManager) : base(entityManager)
        {

        }
    }
}