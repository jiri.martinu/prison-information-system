﻿using System.Collections.Generic;
using PrisonInformationSystem.Models;

namespace PrisonInformationSystem.Utils
{
    public class ObjectIdComparator<T> : IComparer<T>
    {
        public int Compare(T x, T y)
        {
            return (x as BaseEntity).Id > (y as BaseEntity).Id ? 1 : 0;
        }
    }
}
