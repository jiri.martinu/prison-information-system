﻿namespace PrisonInformationSystem.Forms.Controls
{
    public interface IExtraDataControlExtension
    {
        object ExtraData { get; set; }
    }
}