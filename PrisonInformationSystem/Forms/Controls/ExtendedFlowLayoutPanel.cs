﻿using System.Windows.Forms;

namespace PrisonInformationSystem.Forms.Controls
{
    public class ExtendedFlowLayoutPanel : FlowLayoutPanel, IExtraDataControlExtension
    {
        public object ExtraData { get; set; }
    }
}