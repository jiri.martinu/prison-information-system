﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace PrisonInformationSystem.Services.ODM
{
    public class DocumentDatabase
    {
        public Type EntityType { get; set; }

        public XElement EntityRoot { get; set; }

        public DataStorageService DataStorageService { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public DocumentDatabase(Type entityType, DataStorageService dataStorageService)
        {
            EntityType = entityType;
            DataStorageService = dataStorageService;

            EntityRoot = DataStorageService.Root.Element(EntityType.Name + "Database");
        }

        /// <summary>
        /// Get all elements
        /// </summary>  
        public IEnumerable<XElement> GetAll()
        {
            return EntityRoot.Elements(EntityType.Name);
        }

        /// <summary>
        /// Find element by id
        /// </summary>
        public XElement Find(int id)
        {
            return (from elem in GetAll()
                    where (int)elem.Attribute("id") == id
                    select elem).FirstOrDefault();
        }

        /// <summary>
        /// Finds max used id
        /// </summary>  
        public int FindMaxId()
        {
            try {
                return GetAll()
                    .Max(elem => int.Parse(elem.Attribute("id").Value));
            } catch (InvalidOperationException) {
                return 0;
            }
        }

        /// <summary>
        /// Adds element and saves it via DataStorageService
        /// </summary>   
        public void Add(XElement element)
        {
            EntityRoot.Add(element);

            DataStorageService.Flush();
        }

        /// <summary>
        /// Removes element and saves it via DataStorageService
        /// </summary>
        public void Remove(int id)
        {
            XElement element = Find(id);

            if (element != null)  {
                element.Remove();
            }

            DataStorageService.Flush();
        }
    }
}
