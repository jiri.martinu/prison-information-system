﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using PrisonInformationSystem.Models;
using PrisonInformationSystem.Models.ODM;
using PrisonInformationSystem.Utils;
using PrisonInformationSystem.Utils.ODM;
using static PrisonInformationSystem.Utils.ODM.PersistentAttribute;

namespace PrisonInformationSystem.Services.ODM
{
    public class EntityManager
    {
        public AttributesReader AttributesReader { get; set; }

        public ObjectHydratator ObjectHydratator { get; set; }

        public DataStorageService DataStorageService { get; set; }

        public IDictionary<Type, DocumentDatabase> Databases = new Dictionary<Type, DocumentDatabase>();

        public IDictionary<Type, IList<BaseEntity>> EntitySets = new Dictionary<Type, IList<BaseEntity>>();

        public EntityManager(
            AttributesReader attributesReader, 
            ObjectHydratator objectHydratator,
            DataStorageService dataStorageService
        ) {
            AttributesReader = attributesReader;
            ObjectHydratator = objectHydratator;
            DataStorageService = dataStorageService;

            Initialize();
        }

        public void Initialize()
        {
            foreach (Type c in AttributesReader.GetListPersistentClasses()) {
                Databases.Add(c, new DocumentDatabase(c, DataStorageService));
                EntitySets.Add(c, new List<BaseEntity>());
            }

            LoadEntitiesToSets();
            ProcessRelations();
        }

        public void LoadEntitiesToSets()
        {
            foreach (KeyValuePair<Type, DocumentDatabase> database in Databases) {
                foreach (XElement element in database.Value.GetAll()) {
                    EntitySets[database.Key].Add(ObjectHydratator.Hydrate(element) as BaseEntity);
                }
            }
        }

        /// <summary>
        /// Resolve relations (Find them in sets and set references. This could be done in "LoadEntitiesToSets"
        /// but this way it is a lot easier to implement
        /// </summary>
        public void ProcessRelations()
        {
            BaseEntity child;

            foreach (KeyValuePair<Type, IList<BaseEntity>> set in EntitySets) {
                foreach (BaseEntity entity in set.Value) {
                    foreach (Relation relation in entity.Relations) {
                        foreach (int childId in relation.ChildIds) {
                            child = typeof(EntityManager).GetMethod("FindById")
                                .MakeGenericMethod(relation.IsCollection() == true ? relation.GetUnderLayingType() : relation.PropertyInfo.PropertyType)
                                .Invoke(this, new object[] { childId }) as BaseEntity;

                            if (relation.IsCollection() == true) {
                                (relation.PropertyInfo.GetValue(entity) as IList).Add(child);
                            } else {
                                relation.PropertyInfo.SetValue(entity, child);
                            }
                        }
                    }
                }
            }
        }

        public DocumentDatabase GetDatabase<EntityType>()
        {
            return Databases[typeof(EntityType)];
        }

        public IList<BaseEntity> GetAll<EntityType>()
        {
            (EntitySets[typeof(EntityType)] as List<BaseEntity>).Sort(new ObjectIdComparator<BaseEntity>());

            return EntitySets[typeof(EntityType)];
        }

        public BaseEntity FindById<EntityType>(int id)
        {
            return GetAll<EntityType>()
                .Where(e => e.Id == id)
                .FirstOrDefault();
        }

        public BaseEntity Add<EntityType>(BaseEntity entity)
        {
            if (entity.Id == 0) {
                entity.Id = GetDatabase<EntityType>().FindMaxId() + 1;
            }

            GetDatabase<EntityType>().Add(
                ObjectHydratator.Extract(entity)
            );
            GetAll<EntityType>().Add(entity);

            return entity;
        }

        public BaseEntity Update<EntityType>(BaseEntity entity)
        {
            Remove<EntityType>(entity);

            return Add<EntityType>(entity);
        }

        public void Remove<EntityType>(int id, bool cascadeRemove = false)
        {
            BaseEntity entity = FindById<EntityType>(id);

            if (cascadeRemove == true) {
                foreach ((PropertyInfo, PersistentAttribute) property in AttributesReader.GetListPersistentRelationProperties(entity.GetType())) {
                    if (property.Item2.RelationType == RelationTypeNames.OneToMany) {
                        foreach (var child in property.Item1.GetValue(entity) as IList) {
                            RemoveRelated(property.Item1.PropertyType.GenericTypeArguments[0], child as BaseEntity, cascadeRemove);
                        }
                    } else {
                        RemoveRelated(property.Item1.PropertyType, property.Item1.GetValue(entity) as BaseEntity, cascadeRemove);
                    }
                }
            }

            if (entity != null)  {
                GetDatabase<EntityType>().Remove(id);
                GetAll<EntityType>().Remove(entity);
            }
        }

        public void Remove<EntityType>(BaseEntity entity, bool cascadeRemove = false)
        {
            Remove<EntityType>(entity.Id, cascadeRemove);
        }

        public void RemoveRelated(Type type, BaseEntity entity, bool cascadeRemove = false)
        {
            typeof(EntityManager).GetMethod("Remove", new Type[] { typeof(BaseEntity), typeof(bool) })
                .MakeGenericMethod(type)
                .Invoke(this, new object[] { entity, cascadeRemove });
        }
    }
}
