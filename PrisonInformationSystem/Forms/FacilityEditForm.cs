﻿using System;
using System.Windows.Forms;
using Autofac;
using PrisonInformationSystem.Managers;
using PrisonInformationSystem.Models;
using PrisonInformationSystem.Services;

namespace PrisonInformationSystem.Forms
{
    public partial class FacilityEditForm : Form
    {
        public Facility Facility { get; set; }

        public FacilityManager FacilityManager { get; set; }

        public PrisonerManager PrisonerManager { get; set; }
        
        public FacilityPrisonerFacade FacilityPrisonerFacade { get; set; }

        public ILifetimeScope Scope { get; set; }

        public FacilityEditForm(
            Facility facility, 
            FacilityManager facilityManager, 
            PrisonerManager prisonerManager,
            FacilityPrisonerFacade facilityPrisonerFacade,
            ILifetimeScope scope
        ) {
            Facility = facility;
            FacilityManager = facilityManager;
            PrisonerManager = prisonerManager;
            FacilityPrisonerFacade = facilityPrisonerFacade;
            Scope = scope;

            InitializeComponent();
        }

        private void FacilityEditFormLoad(object sender, EventArgs e)
        {
            facilityTextBox.DataBindings.Add(
                "Text", 
                Facility, 
                "Title", 
                false, 
                DataSourceUpdateMode.OnValidation
            );

            prisonersGridView.DataSource = Facility.Prisoners;

            AdjustColumnOrder();

            newToolTip.SetToolTip(newButton, "Adds new prisoner to facility");
            editToolTip.SetToolTip(editButton, "Edit selected prisoner");
            executeToolTip.SetToolTip(executeButton, "Execute selected prisoner");
            moveToolTip.SetToolTip(moveButton, "Move selected prisoner to another facility");
        }

        private void AdjustColumnOrder()
        {
            prisonersGridView.Columns["Id"].DisplayIndex = 0;
            prisonersGridView.Columns["Name"].DisplayIndex = 1;
            prisonersGridView.Columns["Surname"].DisplayIndex = 2;
        }

        private void ExitButtonClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Facility.Title) == true) {
                ShowFacilityAlert();

                return;
            }

            DialogResult = DialogResult.OK;
        }

        private void NewButtonClick(object sender, EventArgs e)
        {
            PrisonerAddEditForm addEditForm = Scope.Resolve<PrisonerAddEditForm>(
                new TypedParameter(typeof(Prisoner), null)
            );

            if (addEditForm.ShowDialog() == DialogResult.OK) {
                FacilityPrisonerFacade.AddPrisonerToFacility(Facility, addEditForm.Prisoner);
            }
        }

        private void EditButtonClick(object sender, EventArgs e)
        {
            if (prisonersGridView.CurrentRow == null) {
                ShowPrisonerAlert();

                return;
            }

            PrisonerAddEditForm addEditForm = Scope.Resolve<PrisonerAddEditForm>(
                new TypedParameter(typeof(Prisoner), prisonersGridView.CurrentRow.DataBoundItem)
            );

            if (addEditForm.ShowDialog() == DialogResult.OK) {
                PrisonerManager.Update(addEditForm.Prisoner);
            }
        }

        private void ExecuteButtonClick(object sender, EventArgs e)
        {
            if (prisonersGridView.CurrentRow == null) {
                ShowPrisonerAlert();

                return;
            }

            FacilityPrisonerFacade.RemovePrisonerWithRelations(prisonersGridView.CurrentRow.DataBoundItem as Prisoner);
        }

        private void MoveButtonClick(object sender, EventArgs e)
        {
            if (prisonersGridView.CurrentRow == null) {
                ShowPrisonerAlert();

                return;
            }

            PrisonerMoveForm moveForm = Scope.Resolve<PrisonerMoveForm>(
                new TypedParameter(typeof(Prisoner), prisonersGridView.CurrentRow.DataBoundItem)
            );

            moveForm.ShowDialog();
        }

        private void FacilityTextBoxTextChanged(object sender, EventArgs e)
        {
            if (facilityTextBox.DataBindings["Text"] != null) {
                facilityTextBox.DataBindings["Text"].WriteValue();
                FacilityManager.Update(Facility);
            }
        }

        private void ShowPrisonerAlert()
        {
            MessageBox.Show(
                "You have choose prisoner!", 
                "Prisoner not selected", 
                MessageBoxButtons.OK
            );
        }

        private void ShowFacilityAlert()
        {
            MessageBox.Show(
                "You can not have facility without title!", 
                "Facility title missing", 
                MessageBoxButtons.OK
            );
        }
    }
}
