﻿using System;
using System.Windows.Forms;
using PrisonInformationSystem.Managers;
using PrisonInformationSystem.Models;

namespace PrisonInformationSystem.Forms
{
    public partial class FacilityAddForm : Form
    {
        public FacilityManager FacilityManager { get; set; }

        public FacilityAddForm(FacilityManager facilityManager)
        {
            FacilityManager = facilityManager;

            InitializeComponent();
        }

        private void SaveButtonClick(object sender, EventArgs e)
        {
            if (facilityTextBox.IsEmpty()) {
                MessageBox.Show(
                    "You have fill name!", 
                    "Unfilled name", 
                    MessageBoxButtons.OK
                );

                return;
            }

            FacilityManager.Add(new Facility { Title = facilityTextBox.Text });

            DialogResult = DialogResult.OK;
        }
    }
}
