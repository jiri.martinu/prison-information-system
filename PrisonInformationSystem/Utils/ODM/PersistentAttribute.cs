﻿using System;

namespace PrisonInformationSystem.Utils.ODM
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]
    public class PersistentAttribute : Attribute
    {
        public enum RelationTypeNames { NotRelation, OneToOne, OneToMany }

        public bool IsId { get; set; } = false;

        public RelationTypeNames RelationType { get; set; } = RelationTypeNames.NotRelation;
    }
}
