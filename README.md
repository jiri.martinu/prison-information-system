# Prison Information System

This is university project on Windows Forms. Lots of things are done just for fun and can be done better.

## Launch

1. Open project in Visual Studio
2. Run project debugging
3. You will get error. Stop debugging
4. Copy "data.xml" and "data_recovery.xml" to "bin\Debug"
5. Run again

## Repository

Link is pointing to private repository. Repository will be change to public after completing university course.
[https://gitlab.com/jiri.martinu/prison-information-system](https://gitlab.com/jiri.martinu/prison-information-system)

## Motivation

I tried to implement simplified hybrid between Doctrine ORM a MongoDB ODM. 
Data is persisted in xml file which simulate MongoDB. Every operation in this file are done using XElements.
Most of dirty work is solved in EntityManager (I know that should be DocumentManager, but it does not sounds right)
and in ObjectHydratator (Does extraction too).