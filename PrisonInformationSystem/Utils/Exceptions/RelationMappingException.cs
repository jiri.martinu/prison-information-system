﻿using System;

namespace PrisonInformationSystem.Utils.Exceptions
{
    class RelationMappingException : Exception
    {
        public RelationMappingException()
        {
        }

        public RelationMappingException(string message) : base(message)
        {
        }

        public RelationMappingException(string message, Exception inner)  : base(message, inner)
        {
        }
    }
}
