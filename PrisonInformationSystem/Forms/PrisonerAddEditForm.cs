﻿using System.Windows.Forms;
using PrisonInformationSystem.Models;

namespace PrisonInformationSystem.Forms
{
    public partial class PrisonerAddEditForm : Form
    {
        public Prisoner Prisoner { get; set; }

        public PrisonerAddEditForm(Prisoner prisoner)
        {
            Prisoner = prisoner;

            InitializeComponent();
        }

        private void PrisonerAddEditFormLoad(object sender, System.EventArgs e)
        {
            ActiveControl = mainTitle;

            if (Prisoner != null) {
                mainTitle.Text = "Edit prisoner";

                nameTextBox.Text = Prisoner.Name;
                surnameTextBox.Text = Prisoner.Surname;
            } else {
                mainTitle.Text = "Add new prisoner";
            }
        }

        private void PrisonerAddEditFormClick(object sender, System.EventArgs e)
        {
            ActiveControl = mainTitle;
        }

        private void SaveButtonClick(object sender, System.EventArgs e)
        {
            if (!(Prisoner is Prisoner)) {
                Prisoner = new Prisoner();
            }

            if (nameTextBox.IsEmpty() || surnameTextBox.IsEmpty()) {
                MessageBox.Show(
                    "You have fill every field!", 
                    "Unfilled field/s", 
                    MessageBoxButtons.OK
                );

                return;
            }

            Prisoner.Name = nameTextBox.Text;
            Prisoner.Surname = surnameTextBox.Text;

            DialogResult = DialogResult.OK;
        }
    }
}
