using System.Collections.Generic;
using PrisonInformationSystem.Models;
using PrisonInformationSystem.Services.ODM;

namespace PrisonInformationSystem.Managers
{
    /// <summary>
    /// Provides basic find/add/edit/remove actions over one "Entity"
    /// </summary>
    public abstract class BaseManager<EntityType>
    {
        public EntityManager EntityManager;

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseManager(EntityManager entityManager) 
        {
            EntityManager = entityManager;
        }

        /// <summary>
        /// Finds all entities
        /// </summary>  
        public IList<BaseEntity> GetAll()
        {
            return EntityManager.GetAll<EntityType>();
        }

        /// <summary>
        /// Finds entity by id
        /// </summary>
        public BaseEntity FindById(int id)
        {
            return EntityManager.FindById<EntityType>(id);
        }

        /// <summary>
        /// Adds new entity and return it
        /// </summary>
        public BaseEntity Add(BaseEntity entity)
        {
            return EntityManager.Add<EntityType>(entity);
        }

        /// <summary>
        /// Updates entity and return it
        /// </summary>
        public BaseEntity Update(BaseEntity entity)
        {
            return EntityManager.Update<EntityType>(entity);
        }

        /// <summary>
        /// Removes entity
        /// </summary>
        public void Remove(BaseEntity entity, bool cascadeRemove = false)
        {
            EntityManager.Remove<EntityType>(entity, cascadeRemove);
        }
    }
}