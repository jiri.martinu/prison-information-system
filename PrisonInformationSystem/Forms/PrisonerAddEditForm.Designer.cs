﻿namespace PrisonInformationSystem.Forms
{
    partial class PrisonerAddEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTitle = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.surnameTextBox = new PrisonInformationSystem.Forms.Controls.ExtendedTextBox();
            this.nameTextBox = new PrisonInformationSystem.Forms.Controls.ExtendedTextBox();
            this.SuspendLayout();
            // 
            // mainTitle
            // 
            this.mainTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.mainTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.mainTitle.Location = new System.Drawing.Point(0, 0);
            this.mainTitle.Margin = new System.Windows.Forms.Padding(0);
            this.mainTitle.Name = "mainTitle";
            this.mainTitle.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.mainTitle.Size = new System.Drawing.Size(174, 45);
            this.mainTitle.TabIndex = 1;
            this.mainTitle.Text = "Prisoner";
            this.mainTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // saveButton
            // 
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.Location = new System.Drawing.Point(12, 133);
            this.saveButton.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.saveButton.Name = "saveButton";
            this.saveButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.saveButton.Size = new System.Drawing.Size(150, 40);
            this.saveButton.TabIndex = 4;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.SaveButtonClick);
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.surnameTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.surnameTextBox.Location = new System.Drawing.Point(12, 94);
            this.surnameTextBox.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.OrigColorHolder = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.surnameTextBox.Placeholder = "Surname";
            this.surnameTextBox.PlaceholderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.surnameTextBox.Size = new System.Drawing.Size(150, 29);
            this.surnameTextBox.TabIndex = 3;
            this.surnameTextBox.Text = "Surname";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.nameTextBox.Location = new System.Drawing.Point(12, 55);
            this.nameTextBox.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.OrigColorHolder = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.nameTextBox.Placeholder = "Name";
            this.nameTextBox.PlaceholderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.nameTextBox.Size = new System.Drawing.Size(150, 29);
            this.nameTextBox.TabIndex = 2;
            this.nameTextBox.Text = "Name";
            // 
            // PrisonerAddEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(174, 185);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.surnameTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.mainTitle);
            this.Name = "PrisonerAddEditForm";
            this.Text = "PrisonerAddEditForm";
            this.Load += new System.EventHandler(this.PrisonerAddEditFormLoad);
            this.Click += new System.EventHandler(this.PrisonerAddEditFormClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label mainTitle;
        private Controls.ExtendedTextBox nameTextBox;
        private Controls.ExtendedTextBox surnameTextBox;
        private System.Windows.Forms.Button saveButton;
    }
}