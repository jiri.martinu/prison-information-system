﻿using PrisonInformationSystem.Utils.ODM;

namespace PrisonInformationSystem.Models
{
    [Persistent]
    public class Prisoner : BaseEntity
    {
        private string name;

        private string surname;

        [Persistent]
        public string Name {
            get {
                return name;
            }
            set {
                name = value;
                NotifyPropertyChanged();
            }
        }

        [Persistent]
        public string Surname {
            get {
                return surname;
            }
            set {
                surname = value;
                NotifyPropertyChanged();
            }
        }

        public override string ToString() {
            return Name + " " + Surname;
        }
    }
}
