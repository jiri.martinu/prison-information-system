﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using PrisonInformationSystem.Utils.ODM;
using static PrisonInformationSystem.Utils.ODM.PersistentAttribute;

namespace PrisonInformationSystem.Models
{
    [Persistent]
    public class Facility : BaseEntity
    {
        private string title;

        [Persistent]
        public string Title {
            get {
                return title;
            }
            set {
                title = value;
                NotifyPropertyChanged();
            }
        }

        [Persistent(RelationType = RelationTypeNames.OneToMany)]
        public IList<Prisoner> Prisoners { get; set; } = new BindingList<Prisoner>();

        public bool HasPrisoner(Prisoner prisoner)
        {
            return Prisoners.Where(p => p.Id == prisoner.Id).FirstOrDefault() is BaseEntity;
        }

        public override string ToString() {
            return Title;
        }
    }
}
