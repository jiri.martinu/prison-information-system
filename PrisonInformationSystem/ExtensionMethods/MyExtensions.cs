using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace PrisonInformationSystem.ExtensionMethods
{
    public static class MyExtensions
    {
        public static string ToHumanReadable(this decimal number)
        {
            return number.ToString("f99").TrimEnd('0');
        }

        public static decimal ToDecimal(this string number)
        {
            CultureInfo cultureInfo = number.Contains(",") ? CultureInfo.CurrentCulture : CultureInfo.InvariantCulture;

            return decimal.Parse(number, cultureInfo);
        }

        public static string GetTimestamp(this DateTime value) {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        public static IEnumerable<T> GetEnumValues<T>() {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static bool HasProperty(this Type obj, string propertyName)
        {
            return obj.GetProperty(propertyName) != null;
        }

        public static void ForEach<T>(this IEnumerable<T> value, Action<T, int> action)
        {
            int index = 0;

            foreach (T item in value) {
                action(item, index);

                index++;
            }
        }

        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) {
                return value;
            }

            return value.Length <= maxLength ? value : value.Substring(0, maxLength); 
        }
    }   
}