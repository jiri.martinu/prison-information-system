﻿namespace PrisonInformationSystem.Managers
{
    public class ManagerProvider
    {
        public FacilityManager FacilityManager;

        public ManagerProvider(FacilityManager facilityManager)
        {
            FacilityManager = facilityManager;
        }

        public BaseManager<EntityType> GetManagerByEntityType<EntityType>()
        {
            return FacilityManager as BaseManager<EntityType>;
        }
    }
}
